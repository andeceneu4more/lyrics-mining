from email.policy import default
from sklearn.pipeline import make_pipeline
from __init__ import train_tf_idf_path, test_tf_idf_path, train_slang_df_path, test_slang_df_path

import numpy as np
import pandas as pd
import pickle
import os
import json

from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier, VotingClassifier, StackingClassifier
from sklearn.preprocessing import MaxAbsScaler
from xgboost import XGBClassifier


from sklearn.metrics import accuracy_score, confusion_matrix
from train_deep import get_exp_dir
from test_deep import plot_and_save_confusions
from scipy import sparse

model_default_configs = {
    "XGBClassifier": {
        "use_label_encoder": False,
        "eval_metric": "mlogloss",
        "verbosity": 1
    },
    "SVM": {
        'kernel': 'rbf',
        'class_weight': 'balanced',
        'verbose': 10,
        'max_iter': 5000
    },
    "RandomForestClassifier": {
        "class_weight": 'balanced'
    }
}

def get_sklearn_model(model_name, **args):
    if model_name == "SVM":
        return SVC(**args)
    elif model_name == "MultinomialNB":
        return MultinomialNB(**args)
    elif model_name == "RandomForestClassifier":
        return RandomForestClassifier(**args)
    elif model_name == "XGBClassifier":
        return XGBClassifier(**args)
    elif model_name in ("VotingClassifier", "StackingClassifier"):
        estimators = []
        for child_name, child_args in args['children_models'].items():
            child_model = get_sklearn_model(child_name, **child_args)
            if child_name == "SVM":
                child_model = make_pipeline(MaxAbsScaler(), child_model)
            
            estimators.append((child_name, child_model))
        model_class = VotingClassifier if model_name == 'VotingClassifier' else StackingClassifier
        return model_class(estimators=estimators)


def fit_the_model(model_name, config, train_lyrics_vec, train_labels):
    model = get_sklearn_model(model_name, **config["args"])
    model.fit(train_lyrics_vec, train_labels)
    return model

def load_the_model(model_filename):
    with open(model_filename, 'rb') as fin:
        model = pickle.load(fin)
    return model

def main(config):
    """loads the dataframe 
    """
    df_train = pd.read_csv(train_slang_df_path)
    train_labels = np.array(df_train['genre_id'])

    df_test = pd.read_csv(test_slang_df_path)
    test_labels = np.array(df_test['genre_id'])

    train_lyrics_vec = sparse.load_npz(train_tf_idf_path)
    test_lyrics_vec = sparse.load_npz(test_tf_idf_path)

    model_name = config["model_name"]

    weigths_path = config.get("weigths_path")
    if weigths_path is None:
        exp_dir = get_exp_dir(model_name)
        model = fit_the_model(model_name, config, train_lyrics_vec, train_labels)
    else:
        exp_dir = os.path.dirname(weigths_path)
        model = load_the_model(weigths_path)

    test_pred_labels = model.predict(test_lyrics_vec)
    accuracy = accuracy_score(test_labels, test_pred_labels)
    print(f"Light model finished with accuracy = {accuracy}")
    config["validation_acc"] = accuracy

    conf_matrix = confusion_matrix(test_labels, test_pred_labels)
    plot_and_save_confusions(exp_dir, conf_matrix)

    model_filename = os.path.join(exp_dir, 'best.pkl')
    with open(model_filename, 'wb') as fout:
        pickle.dump(model, fout)

    config_filename = os.path.join(exp_dir, 'config.json')
    with open(config_filename, 'w') as fout:
        json.dump(config, fout, indent=4)

def run(model_name='XGBClassifier', children_models=[], weigths_path=None):
    """ makes a configuration with default arguments if from the 3 arguments from input

    Args:
        model_name (str, optional): name of the model used in get_sklearn_model function. Defaults to 'XGBClassifier'.
        children_models (list, optional): list of model names used in "VotingClassifier" or "StackingClassifier". Defaults to [].
        weigths_path ([type], optional): If you want just to test a model already trained, this is the path to the weights of the model stored. Defaults to None.
    """
    
    if model_name in ("VotingClassifier", "StackingClassifier"):
        arguments = {
            "children_models": {
                child_model: model_default_configs[child_model] \
                             for child_model in children_models
            }
        }
    else:
        arguments = model_default_configs

    main({
        "model_name": model_name,
        "args": arguments,
        "weigths_path": weigths_path
    })

if __name__ == "__main__":
    run()    
