import os
import json
import torch
import seaborn as sns
import argparse

from train_deep import get_model_by_name, get_dataloaders, device
from sklearn.metrics import accuracy_score, confusion_matrix

def load_trained_model(config, experiment_folder):
    model_name = config.get("model_name")
    model = get_model_by_name(model_name)
        
    weights_path = os.path.join(experiment_folder, 'best.pth')
    state_dict = torch.load(weights_path)
    model.load_state_dict(state_dict["model"])
    model.eval()
    
    return model

def run_inference_deep(model, dataloader):
    pred_labels = []
    gt_labels = []
    for i, batch in enumerate(dataloader):
        input_ids, labels = batch
        input_ids = input_ids.to(device)
        gt_labels += labels.tolist()

        with torch.no_grad():
            output = model(input_ids)

        out_labels = torch.argmax(output, axis=1)
        pred_labels += out_labels.tolist()

        print(f"Iterating throught the dataset {i}/{len(dataloader)}", end='\r')
    print()

    accuracy = accuracy_score(gt_labels, pred_labels)
    conf_matrix = confusion_matrix(gt_labels, pred_labels)
    return accuracy, conf_matrix

def plot_and_save_confusions(experiment_folder, conf_matrix):
    confusions_filename = os.path.join(experiment_folder, "confusions.png")
    sns_plot = sns.heatmap(conf_matrix, annot=True, fmt='', cmap='Blues')
    fig = sns_plot.get_figure()
    fig.savefig(confusions_filename)

def main(experiment_folder):
    config_filename = os.path.join(experiment_folder, 'config.json')

    with open(config_filename, 'r') as fin:
        config = json.load(fin)

    model = load_trained_model(config, experiment_folder)
    dataloaders = get_dataloaders(config)
    test_dataloader = dataloaders.get("valid")
    
    accuracy, conf_matrix = run_inference_deep(model, test_dataloader)
    print(f"Deep model finished with accuracy = {accuracy}")
    plot_and_save_confusions(experiment_folder, conf_matrix)

    config["validation_acc"] = accuracy
    with open(config_filename, 'w') as fout:
        json.dump(config, fout, indent=4)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--experiment-folder', type=str, help='the name of the folder experiment')
    args = parser.parse_args()
    config = vars(args)
    main(config['experiment-folder'])