from torch.utils.data import Dataset
from __init__ import train_tf_idf_path, test_tf_idf_path, train_slang_df_path, test_slang_df_path
from scipy import sparse

import pandas as pd
import pdb
import torch

class TfIdfDataset(Dataset):
    def __init__(self, scope):
        super().__init__()

        if scope == 'train':
            self.nz_path = train_tf_idf_path
            self.df_path = train_slang_df_path
        else:
            self.nz_path = test_tf_idf_path
            self.df_path = test_slang_df_path

        df = pd.read_csv(self.df_path)
        self.labels = df["genre_id"].tolist()
        self.sparse_entries = sparse.load_npz(self.nz_path)
    
    def __len__(self):
        return self.sparse_entries.shape[0]

    def __getitem__(self, index):
        sparse_line = self.sparse_entries[index]
        tensor_line = torch.tensor(sparse_line.toarray()[0]).float()

        label = self.labels[index]
        label = torch.tensor(label)
        return tensor_line, label

