import os

data_folder = 'data'
picture_folder = 'pictures'
logs_folder = 'logs'

for folder in [data_folder, picture_folder, logs_folder]:
    if os.path.exists(folder):
        os.makedirs(folder)

train_df_path = os.path.join(data_folder, 'Lyrics-Genre-Train.csv')
train_slang_df_path = os.path.join(data_folder, 'Lyrics-Slang-Genre-Train.csv')

test_df_path = os.path.join(data_folder, 'Lyrics-Genre-Test-GroundTruth.csv')
test_slang_df_path = os.path.join(data_folder, 'Lyrics-Slang-Genre-Test-GroundTruth.csv')

class_weights_path = os.path.join(data_folder, 'train_class_wigths.npy')

train_tf_idf_path = os.path.join(data_folder, 'train_tf_idfs.npz')
test_tf_idf_path = os.path.join(data_folder, 'test_tf_idfs.npz')
tf_idf_words_path = os.path.join(data_folder', 'tfidf_words.json')