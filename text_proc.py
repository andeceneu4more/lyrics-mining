import nltk
# nltk.download('punkt')
# nltk.download('words')
# nltk.download('stopwords')

import string
import pandas as pd
import json
import os
import pdb

from nltk import word_tokenize
from nltk.corpus import words, stopwords
from nltk.stem import PorterStemmer
stemmer = PorterStemmer()

from __init__ import train_slang_df_path, test_slang_df_path, train_tf_idf_path, test_tf_idf_path, tf_idf_words_path, train_df_path, test_df_path
from sklearn.preprocessing import LabelEncoder
from sklearn.feature_extraction.text import TfidfVectorizer
from scipy import sparse
from statistics import export_class_weights, plot_class_distribution, plot_genres_word_clouds

special_words_list = ['yaya', 'ycie', 'ye', 'yea', 'yeah', 'Imma']
all_words_dict = {word: True for word in words.words()}
stopwords_dict = {word: True for word in stopwords.words('english')}

def is_slang_word(word):
    if stopwords_dict.get(word, False):
        return True
    if all_words_dict.get(word, False):
        return True
    if word in special_words_list:
        return True
    return False

def get_slang_sentence(input_sent):
    tokens = word_tokenize(input_sent)
    return ' '.join([token for token in tokens \
                     if is_slang_word(token.lower())])

def get_meaning_sentence(input_sent):
    tokens = word_tokenize(input_sent)
    return ' '.join([stemmer.stem(token) for token in tokens \
                     if not stopwords_dict.get(token.lower(), False) and token not in string.punctuation])

def apply_slang_on_lyrics(df):
    slangs_column = []
    meaning_column = []
    for i in range(len(df)):
        full_lyrics = df.loc[i, "Lyrics"]
        slang_lyrics = get_slang_sentence(full_lyrics)
        slangs_column.append(slang_lyrics)

        meaning_lyrics = get_meaning_sentence(full_lyrics)
        meaning_column.append(meaning_lyrics)
        if i % 100 == 0:
            print(f"Slangification progress {i}/{len(df)}", end='\r')

    df["slang_lyrics"] = slangs_column
    df["meaning_lyrics"] = meaning_column

def apply_genres_encoding(df):
    encoder = LabelEncoder()
    df["genre_id"] = encoder.fit_transform(df["Genre"])   

def get_tf_idf_vec(df_train, df_test, target_column):
    df_train = df_train.fillna("")
    train_lyrics = df_train[target_column].tolist()

    df_test = df_test.fillna("")
    test_lyrics = df_test[target_column].tolist()
    
    vectorizer = TfidfVectorizer(analyzer='word', lowercase=True, ngram_range=(1, 2), max_features=10000)
    train_lyrics_vec = vectorizer.fit_transform(train_lyrics)
    test_lyrics_vec = vectorizer.transform(test_lyrics)
    return train_lyrics_vec, test_lyrics_vec, vectorizer


def apply_transformations(input_df_path, slang_df_path):
    if os.path.exists(slang_df_path):
        df = pd.read_csv(slang_df_path)
    else:
        df = pd.read_csv(input_df_path)
        apply_slang_on_lyrics(df)
        apply_genres_encoding(df)
        df.to_csv(slang_df_path)
    return df

def preprocess_and_run_stats(plot_them=False):
    df_train = apply_transformations(train_df_path, train_slang_df_path)
    export_class_weights(df_train)
    
    if plot_them:
        plot_class_distribution(df_train)
        plot_genres_word_clouds(df_train)

    df_test = apply_transformations(test_df_path, test_slang_df_path)
    return df_train, df_test

def main():
    df_train, df_test = preprocess_and_run_stats()

    train_lyrics_vec, test_lyrics_vec, vectorizer = get_tf_idf_vec(df_train, df_test, "meaning_lyrics")
    sparse.save_npz(train_tf_idf_path, train_lyrics_vec)
    sparse.save_npz(test_tf_idf_path, test_lyrics_vec)
    
    vocab = {key: int(value) for key, value in vectorizer.vocabulary_.items() if type(value) != str}
    with open(tf_idf_words_path, 'w') as fout:
        json.dump(vocab, fout, indent=4, sort_keys=True)

if __name__ == "__main__":
    main()    
