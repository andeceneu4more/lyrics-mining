from transformers import BertForSequenceClassification
import torch.nn as nn

class BertForLyricsClassification(BertForSequenceClassification):
    def __init__(self, config):
        super().__init__(config)

        self.classifier = nn.Sequential(
            nn.Linear(768, 256),
            nn.Dropout(p=0.2),

            nn.GELU(),
            nn.Linear(256, config.num_clases),
            nn.Softmax(dim=-1)
        )

    def forward(self, **kwargs):
        output = super().forward(**kwargs)
        return output.logits

class ANNForTfIdfClassification(nn.Module):
    def __init__(self, num_clases):
        super().__init__()
        self.num_classes = num_clases
        self.fc = nn.Sequential(
            nn.Linear(10000, 2500),
            nn.Dropout(p=0.2),
            nn.GELU(),

            nn.Linear(2500, 512),
            nn.Dropout(p=0.1),
            nn.GELU(),

            nn.Linear(512, num_clases),
            nn.Softmax(dim=-1)
        )
    
    def forward(self, x):
        return self.fc(x)