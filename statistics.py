import pandas as pd
import numpy as np
import os
import pdb

from __init__ import picture_folder, class_weights_path

from wordcloud import WordCloud
from collections import Counter
import matplotlib.pyplot as plt
from nltk import word_tokenize

def plot_class_distribution(df):
    ax = df["Genre"].hist(figsize=(15, 10))
    figure = ax.get_figure()
    fig_filename = os.path.join(picture_folder, 'genres_distro.png')
    figure.savefig(fig_filename)

def plot_word_cloud(texts, key):
    entire_text = ' '.join(texts)
    all_tokens = word_tokenize(entire_text)
    tokens_freq = Counter(all_tokens)

    wordcloud = WordCloud(width = 800, height = 800, background_color = 'white',  min_font_size = 10)
    wordcloud = wordcloud.generate_from_frequencies(tokens_freq)

    plt.imshow(wordcloud) 
    plt.axis("off") 
    plt.tight_layout(pad = 0)
    fig_filename = os.path.join(picture_folder, f"{key}_distro.png")
    plt.savefig(fig_filename)

def plot_genres_word_clouds(df):
    labels = pd.unique(df["Genre"]).tolist()
    for label in labels:
        these_lyrics = df.loc[df["Genre"] == label, "Lyrics"].tolist()
        these_slang_lyrics = df.loc[df["Genre"] == label, "slang_lyrics"].tolist()

        plot_word_cloud(these_lyrics, f'{label}_normal')
        plot_word_cloud(these_slang_lyrics, f'{label}_slang')

def export_class_weights(df):
    weigths = np.zeros(10)
    for genre_id in df['genre_id'].tolist():
        weigths[genre_id] += 1
    weigths /= np.sum(weigths)
    np.save(class_weights_path, weigths)

