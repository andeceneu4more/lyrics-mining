from cv2 import exp
from custom_models import BertForLyricsClassification, ANNForTfIdfClassification
from LyricsDataset import LyricsDataset
from TfIdfDataset import TfIdfDataset
from torch.utils.data import DataLoader
from torch.optim.lr_scheduler import ReduceLROnPlateau
from torch.utils.tensorboard import SummaryWriter
from torch.optim import Adam
from glob import glob

import pdb
import torch
import torch.nn as nn
import os
import numpy as np
import json
import argparse
from __init__ import class_weights_path, logs_folder

device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

def get_lr(optimizer):
    """ extracts the learning rate used inside the optimizer
    """
    for param_group in optimizer.param_groups:
        return param_group['lr']

def compute_the_loss(model, batch, optimizer, criterion, phase):
    """ computes the loss of the model from a batch
    """
    input_ids, genre_id = batch
    input_ids = input_ids.to(device)
    genre_id = genre_id.to(device)

    optimizer.zero_grad()
    with torch.set_grad_enabled(phase == 'train'):
        output = model(input_ids)
        loss = criterion(output, genre_id)

        if phase == 'train':
            loss.backward()
            optimizer.step()
    return loss

def log_in_tensorboard(writer, average_loss, epoch, phase):
    """ logs in the tensorboard writer
    """
    writer.add_scalar(f'{phase.title()}/Loss', average_loss, epoch)
    writer.flush()

def save_and_advance(model, optimizer, lr_sched, epoch, exp_dir, average_loss, best_loss):
    """ stores the model if it exceeds the best validation accuracy 
    """
    if average_loss < best_loss:
        best_loss = average_loss
        saving_path = os.path.join(exp_dir, 'best.pth')
        torch.save({
                'model': model.state_dict(),
                'optimizerrizer': optimizer.state_dict(),
                'lr_sched': lr_sched.state_dict(),
                'epoch': epoch,
                'validation_loss': best_loss
            }, saving_path)
    try:
        lr_sched.step()
    except:
        lr_sched.step(average_loss)
    return best_loss, lr_sched

def train_epoch(epoch, model, criterion, dataloaders, optimizer, lr_sched, writer, exp_dir, best_loss):
    for phase in ['train', 'valid']:
        epoch_losses = []

        if phase == 'train':
            model.train()
        else:
            model.eval()

        for i, batch in enumerate(dataloaders[phase]):
            loss = compute_the_loss(model, batch, optimizer, criterion, phase)
            epoch_losses.append(loss.item())
            average_loss = np.mean(epoch_losses)
            lr = get_lr(optimizer)

            if (i + 1) % 10 == 0:
                loading_percentage = int(100 * (i+1) / len(dataloaders[phase]))
                print(f'{phase}ing epoch {epoch}, iter = {i+1}/{len(dataloaders[phase])} ' + \
                    f'({loading_percentage}%), loss = {loss}, average_loss = {average_loss} ' + \
                    f'learning rate = {lr}', end='\r')
        print()        
        if phase == 'valid':
            best_loss, lr_sched = save_and_advance(model, optimizer, lr_sched, epoch, exp_dir, average_loss, best_loss)
        
        log_in_tensorboard(writer, average_loss, epoch, phase)
    return best_loss

def get_dataloaders(config):
    """ builds the dataloaders for train and validation; based on the model name, they can consist of tf-idfs (for ann_plain) or bert tokens, otherwise
    """
    if config.get("model_name").startswith("ann"):
        train_dataset = TfIdfDataset('train')
        valid_dataset = TfIdfDataset('valid')
    else:
        train_dataset = LyricsDataset('train')
        valid_dataset = LyricsDataset('valid')
    
    train_dataloader = DataLoader(train_dataset, batch_size=config["batch_size"], \
                                     shuffle=True, num_workers=4)
    valid_dataloader = DataLoader(valid_dataset, batch_size=config["batch_size"], \
                                     shuffle=False, num_workers=4)
    return {
        'train': train_dataloader,
        'valid': valid_dataloader
    }

def get_model_by_name(model_name, num_clases=10):
    """ instantiates the model by name
    """
    if model_name.startswith('bert'):
        model = BertForLyricsClassification.from_pretrained(model_name, num_clases=num_clases)
        for param in model.bert.parameters():
            param.requires_grad = False
    
    elif model_name == 'ann_plain':
        model = ANNForTfIdfClassification(num_clases)
    return model.to(device)

def get_exp_dir(model_name):
    experiments_re = os.path.join(logs_folder, f'{model_name}*')
    n_experments = len(glob(experiments_re))
    exp_dir = os.path.join(logs_folder, f'{model_name}_{n_experments}')
    os.makedirs(exp_dir)
    return exp_dir

def main(config):
    dataloaders = get_dataloaders(config)

    model_name = config.get("model_name")
    model = get_model_by_name(model_name)
    
    if config["optimizer"] == "Adam":
        optimizerr = Adam(model.parameters(), lr=0.0001, betas=(0.9, 0.98), eps=1e-9)
    lr_sched = ReduceLROnPlateau(optimizerr, factor = 0.1, patience = 3, mode = 'min')

    exp_dir = get_exp_dir(model_name)
    writer = SummaryWriter(os.path.join(exp_dir, 'runs'))
    
    config_filename = os.path.join(exp_dir, 'config.json')
    with open(config_filename, 'w') as fout:
        json.dump(config, fout)

    class_weigths = torch.tensor(np.load(class_weights_path)).float().to(device)
    criterion = nn.CrossEntropyLoss(weight=class_weigths)
    best_loss = 5000
    for epoch in range(config["n_epochs"]):
        best_loss = train_epoch(epoch, model, criterion, dataloaders, optimizerr, lr_sched, writer, exp_dir, best_loss)
    return exp_dir

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--model-name', default='bert-base-uncased', type=str, \
                        help='the name of the deep model from the list: can be one from the hugging face lib, which name starts with \'bert\' or ann_plain')
    parser.add_argument('--batch-size', default=32, type=int, \
                        help='batch size needed for the dataloader, default is 32')
    parser.add_argument('--optimizer', default='Adam', type=str, \
                        help='name of the optimizer, default is Adam')
    parser.add_argument('--n-epochs', default=16, type=int, \
                        help='name of training epochs, default is 16')                    
    args = parser.parse_args()
    config = vars(args)
    main(config)