from torch.utils.data import Dataset
from __init__ import train_slang_df_path, test_slang_df_path
from transformers import BertTokenizer

import pandas as pd
import torch
import pdb

class LyricsDataset(Dataset):
    def __init__(self, scope, max_len=150):
        super().__init__()

        if scope == 'train':
            self.df_path = train_slang_df_path
        else:
            self.df_path = test_slang_df_path

        self.df = pd.read_csv(self.df_path)
        self.df = self.df.fillna("")
    
        self.tokenizer = BertTokenizer.from_pretrained('bert-base-uncased')
        self.max_len = max_len
    
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self, index):
        entry = self.df.iloc[index]
        lyrics = entry["slang_lyrics"]
        genre_id = entry["genre_id"]

        input_ids = self.tokenizer.encode(lyrics, return_tensors='pt', truncation=True, \
                                        padding='max_length', max_length=self.max_len)
        return input_ids[0], torch.tensor(genre_id)